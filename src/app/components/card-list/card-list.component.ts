import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/interface/interface';
import { products } from '../../db/data';
import { MatDialog } from '@angular/material/dialog';
import { ModalDialogComponent } from '../modal-dialog/modal-dialog.component';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {

  public items: Card[] = products;

  constructor(public dialog: MatDialog) { }

  public openDialog(item: Card, id: number): void {

    let dialogRef = this.dialog.open(ModalDialogComponent, {
      data: {
        item: item
      }
    });

    dialogRef.componentInstance.onRemove.subscribe(() => {
      this.removeItem(id);
    });
  }

  public removeItem(id: number): void {
    
    const newItems = this.items.filter((item: Card) => item.id !== id);

    this.items = [...newItems];
  }

  ngOnInit(): void {
  }

}


