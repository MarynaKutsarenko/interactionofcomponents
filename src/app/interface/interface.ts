export interface Card {
   "id": number,
   "title": string,
   "photo": string,
   "description": string,
   "price": number
}