export const products = [
   {
      "id": 0,
      "title": "Batai NIKE",
      "photo": "../../assets/images/db/Nike.jpeg",
      "description": "Air Zoom Superrep  Barely Rose",
      "price": 200
   },
      {
      "id": 1,
      "title": "Diesel",
      "photo": "../../assets/images/db/Diesel_Yellow.jpeg",
      "description": "Diesel кеды S-Clever",
      "price": 120
   },
   {
      "id": 2,
      "title": "LIU JO",
      "photo": "../../assets/images/db/Liu_Jo.jpeg",
      "description": "Yulia Grey/Fuchsia S19B4",
      "price": 105
   },
   {
      "id": 3,
      "title": "Puma Cali",
      "photo": "../../assets/images/db/Puma.webp",
      "description": "Puma Cali Logo",
      "price": 80
   },
   {
      "id": 4,
      "title": "CLJD",
      "photo": "../../assets/images/db/cljd.jpeg",
      "description": "CLJD 005-0202 M4",
      "price": 320
   },
   {
      "id": 5,
      "title": "PREGO",
      "photo": "../../assets/images/db/prego.jpeg",
      "description": "PREGO 023121, color beige",
      "price": 75
   },
]