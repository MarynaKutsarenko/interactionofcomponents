import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Card } from 'src/app/interface/interface';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.scss']
})
export class ModalDialogComponent implements OnInit {

  @Input() item: Card = {} as Card;
  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  constructor(private dialogRef: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }


  public removeItem(): void {
    this.onRemove.emit(this.item.id); 
  }

  ngOnInit() {
  }

}