import { Component, Input, OnInit } from '@angular/core';
import { Card } from 'src/app/interface/interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() item: Card = {} as Card;

  constructor() { }

  ngOnInit(): void {
  }
}


